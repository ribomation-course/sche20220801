cmake_minimum_required(VERSION 3.14)
project(exceptions)

set(CMAKE_CXX_STANDARD 17)

add_executable(exception-tracking
        trace.hxx trace.cxx
        exception-tracking.cxx)

add_executable(unknown-exception
        trace.hxx trace.cxx
        unknown-exception.cxx)

add_executable(system-errors
        system-errors.cxx)

add_executable(catch-app-failures
        trace.hxx trace.cxx
        catch-app-failures.cxx)
target_compile_options(catch-app-failures PRIVATE
    -std=c++14 -Wall -Wextra -fnon-call-exceptions
)



#pragma once
#include <iostream>
#include <string>

namespace ribomation {
    using namespace std;
    using namespace std::literals;

    class Trace {
        const string name;
        static int   level;
        string tab() { return string(2 * level, ' '); }
    public:
        Trace(string n) : name{n} {
            print("enter", name);
            ++level;
        }

        ~Trace() {
            --level;
            print("exit ", name);
        }

        void print(string msg) { print("msg  ", msg); }
        void print(string prefix, string msg) {
            cout << prefix << ": " << tab() << msg << endl;
        }
    };
}

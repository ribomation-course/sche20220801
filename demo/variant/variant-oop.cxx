#include <iostream>
#include <string>
#include <variant>
#include <vector>
#include <cmath>

using namespace std;
using namespace std::literals;

struct Shape {
    const string type;

    Shape(string const& type) : type{type} {
        cout << "Shape(" << type << ") @ " << this << endl;
    }

    Shape(const Shape& that) : type{that.type} {
        cout << "Shape(& that=" << &that << ", type=" << type << ") @ " << this << endl;
    }

    ~Shape() {
        cout << "~Shape(" << type << ") @ " << this << endl;
    }

    virtual double area() = 0;
};

struct Square : Shape {
    Square(int side) : Shape{"square"s}, side{side} {}

    double area() override { return side * side; }

private:
    int side;
};

struct Rect : Shape {
    Rect(int width, int height) : Shape{"rect"s}, width{width}, height{height} {}

    double area() override { return width * height; }

private:
    long width;
    long height;
};

struct Circle : Shape {
    Circle(int r) : Shape{"circle"s}, radius{static_cast<long double>(r)} {}

    double area() override { return M_PI * radius * radius; }

private:
    long double radius;
};

using ShapeType = variant<Square, Rect, Circle>;

template<class... Ts>
struct match : Ts ... {
    using Ts::operator ()...;
};
template<class... Ts> match(Ts...) -> match<Ts...>;


int main(int argc, char** argv) {
    cout << "sizeof(Shape): " << sizeof(Shape) << endl;
    cout << "sizeof(Square): " << sizeof(Square) << endl;
    cout << "sizeof(Rect): " << sizeof(Rect) << endl;
    cout << "sizeof(Circle): " << sizeof(Circle) << endl;

    {
        cout << "------\n";
        auto shapes = vector<ShapeType>{
                Square{3}, Rect{2, 5}, Circle{4},
                //Square{7}, Rect{3, 9}, Circle{11},
        };
        cout << "------\n";
        for (auto& var : shapes) {
            visit([](auto&& s) { cout << "area(" << s.type << "): " << s.area() << endl; }, var);
        }
    }

    {
        cout << "------\n";
        auto shapes = vector<ShapeType>{};
        shapes.reserve(3);
        shapes.emplace_back(Square{2});
        shapes.emplace_back(Rect{2, 5});
        shapes.emplace_back(Circle{3});
        cout << "------\n";
        for (auto& var : shapes) {
            visit([](auto&& s) { cout << "area(" << s.type << "): " << s.area() << endl; }, var);
        }
    }

    {
        cout << "------\n";
        auto shapes = vector<ShapeType>{
                Square{7}, Rect{3, 9}, Circle{11}
        };
        cout << "------\n";
        for (auto& var : shapes) {
            visit(match{
                    [](Square& sq) { cout << "SQRE: " << sq.area(); },
                    [](Rect& r) { cout << "RECT: " << r.area(); },
                    [](Circle& c) { cout << "CIRC: " << c.area(); }
            }, var);
            cout << endl;
        }
    }


    return 0;
}


#pragma once
#include <iostream>
#include <string>
#include <memory_resource>

namespace ribomation {
    using std::pmr::polymorphic_allocator;
    using std::ostream;

    class Person {
        std::pmr::string name;
        unsigned         age;
    public:
        using allocator_type = polymorphic_allocator<char>;

        Person(const std::string& name, unsigned age, allocator_type alloc = {})
                : name{name, alloc}, age{age} {}
        Person(const Person& that, allocator_type alloc)
                : name{that.name, alloc}, age{that.age} {}
        Person(Person&& that, allocator_type alloc)
                : name{move(that.name), alloc}, age{that.age} {}
        ~Person() = default;

        std::string getName() const {
            return {name.cbegin(), name.cend()};
        }
        void setName(const std::string& value) {
            Person::name = value;
        }

        unsigned getAge() const { return age; }
        void     setAge(unsigned value) { Person::age = value; }

        friend ostream& operator <<(ostream& os, const Person& p) {
            return os << "Person{" << p.name << ", " << p.age << "}";
        }
    };
}



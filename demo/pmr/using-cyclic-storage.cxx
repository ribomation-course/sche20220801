#include <iostream>
#include <vector>
#include "cyclic_buffer_resource.hxx"

using std::cout;
using std::data;
using std::size;
using std::pmr::vector;
using ribomation::cyclic_buffer_resource;

int main() {
    auto const N      = 35U;
    char       storage[N];
    auto       memory = cyclic_buffer_resource{data(storage), size(storage)};

    auto      chars = vector<char>{&memory};
    chars.reserve(10);
    for (auto ch    = 'a'; ch <= 'z'; ++ch) chars.push_back(ch);
    storage[N - 1] = '\0';
    cout << "#storage [" << storage << "]\n";

    return 0;
}



#pragma once

#include <memory_resource>
#include <cstdio>

namespace ribomation {
    using std::pmr::memory_resource;

    struct cyclic_buffer_resource : memory_resource {
        cyclic_buffer_resource(void* storage, size_t size) noexcept
                : storage{storage},
                  storageEnd{ADD(storage, size)},
                  nextAddress{storage} {}

        ~cyclic_buffer_resource() override = default;
        cyclic_buffer_resource(const cyclic_buffer_resource&) = delete;
        cyclic_buffer_resource& operator =(const cyclic_buffer_resource&) = delete;

    protected:
        void* do_allocate(size_t numBytes, size_t alignment) override {
            printf("do_allocate(%ld, %ld) nxt=%ld\n", numBytes, alignment, nextAddress);
            if (ADD(nextAddress, numBytes) >= storageEnd) {
                printf("  wrap-around nxt=%ld\n", nextAddress);
                nextAddress = storage;
            }
            auto blockStart = nextAddress;
            printf("  block-start=%ld\n", blockStart);
            nextAddress = ADD(nextAddress, numBytes);
            return blockStart;
        }

        void do_deallocate(void* __p, size_t __bytes, size_t __alignment) override {
            // nothing
        }

        [[nodiscard]]
        bool do_is_equal(const memory_resource& that) const noexcept override {
            return this == &that;
        }

    private:
        void* const storage;
        void* const storageEnd;
        void* nextAddress;
        void* ADD(void* base, size_t offset) const {
            return static_cast<char*>(base) + offset;
        }
    };
}


cmake_minimum_required(VERSION 3.14)
project(pmr)

set(CMAKE_CXX_STANDARD 17)

add_executable(using-monotonic-storage using-monotonic-storage.cxx)
add_executable(using-pool-storage using-pool-storage.cxx)
add_executable(understanding-sso understanding-sso.cxx)
add_executable(using-cyclic-storage
        cyclic_buffer_resource.hxx
        using-cyclic-storage.cxx
        )
add_executable(persons-app
        person.hxx
        persons-app.cxx
        )

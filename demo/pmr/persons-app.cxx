#include <vector>
#include "person.hxx"

using std::cout;
using std::endl;
using std::data;
using std::size;
using std::to_string;
using namespace std::string_literals;

using std::pmr::monotonic_buffer_resource;
using std::pmr::null_memory_resource;
using std::pmr::vector;

using ribomation::Person;

int main() {
    char storage[2 * 1024];
    auto memory = monotonic_buffer_resource{data(storage), size(storage), null_memory_resource()};
    set_default_resource(&memory);

    {
        auto persons = vector<Person>{};

        for (auto k = 0U; k < 10U; ++k) {
            auto name = "nisse-"s + to_string(k + 1);
            auto age  = 20U + k % 50;
            persons.emplace_back(name, age);
        }

        for (auto const& p : persons) cout << p << endl;
    }

    return 0;
}



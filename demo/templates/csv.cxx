#include <iostream>
#include <iomanip>
#include <sstream>
#include <string>
#include <type_traits>

using namespace std;
using namespace std::literals;

template<typename T>
string toString(T x, int decimals) {
    static_assert(is_arithmetic_v<T>, "toString(T), T is not numeric");
    ostringstream buf;
    buf << fixed << setprecision(decimals) << x;
    return buf.str();
}

template<typename T>
string asString(T arg) {
    //cerr << __PRETTY_FUNCTION__ << endl;
    if constexpr (is_same_v<T, bool>) return arg ? "T" : "F";
    if constexpr (is_arithmetic_v<T>) return toString(arg, 2);
    if constexpr (is_convertible_v<T, std::string>) return arg;
    return "??";
}

template<auto SEP = ';', typename First, typename... Rest>
string CSV(const First& first, const Rest& ... rest) {
    auto append = [&](const auto& arg) {
        return SEP + asString(arg);
    };
    return (asString(first) + ... + append(rest));
}

int main() {
    auto name   = "Anna Conda"s;
    auto age    = 42U;
    auto weight = 53.7;
    auto female = true;
    cout << "CSV: \"" << CSV(name, age, weight, female, "A char* string") << "\"\n";
    cout << "CSV: \"" << CSV<'#'>(name, age, weight, female, "A char* string") << "\"\n";
    return 0;
}



#include <iostream>
#include <memory>
#include "person.hxx"

using namespace ribomation;
using namespace std::literals;
using std::unique_ptr;
using std::shared_ptr;

void use_unique_ptr();
auto func2(unique_ptr<Person> q) -> unique_ptr<Person>;
auto func1(unique_ptr<Person> q) -> unique_ptr<Person>;

void use_shared_ptr();
auto func2(shared_ptr<Person> q) -> shared_ptr<Person>;
auto func1(shared_ptr<Person> q) -> shared_ptr<Person>;


int main() {
    use_unique_ptr();
    cout << "------\n";
    use_shared_ptr();
    return 0;
}

void use_unique_ptr() {
    auto anna = std::make_unique<Person>("Anna Conda"s, 42);
    cout << "[use_unique_ptr] anna: " << *anna << endl;

    auto p = func1(std::move(anna));

    cout << "[use_unique_ptr] p: " << *p << endl;
    cout << "[use_unique_ptr] anna: " << anna.get() << endl;
}

auto func1(unique_ptr<Person> q) -> unique_ptr<Person> {
    q->incrAge();
    cout << "[func1] q: " << *q << endl;
    return func2(std::move(q));
}

auto func2(unique_ptr<Person> q) -> unique_ptr<Person> {
    q->incrAge();
    cout << "[func2] q: " << *q << endl;
    return q;
}


void use_shared_ptr() {
    auto justin = std::make_shared<Person>("Justin Time"s, 37);
    cout << "[use_shared_ptr] justin: " << *justin << " [" << justin.use_count() << "]" << endl;
    {
        auto p = func1(justin);
        cout << "[use_shared_ptr] p: " << *p << " [" << p.use_count() << "]" << endl;
    }
    cout << "[use_shared_ptr] justin: " << *justin << " [" << justin.use_count() << "]" << endl;
}

auto func1(shared_ptr<Person> q) -> shared_ptr<Person> {
    q->incrAge();
    cout << "[func1] q: " << *q << " [" << q.use_count() << "]" << endl;
    return func2(q);
}

auto func2(shared_ptr<Person> q) -> shared_ptr<Person> {
    q->incrAge();
    cout << "[func2] q: " << *q << " [" << q.use_count() << "]" << endl;
    return q;
}

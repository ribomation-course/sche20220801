#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <string_view>
#include <map>
#include <chrono>
#include <tuple>
#include <stdexcept>
#include <memory>

using std::cout;
using std::string;
using std::string_view;
using std::unique_ptr;
using namespace std::literals;
namespace chr = std::chrono;

using WordFreqs = std::map<string_view, unsigned>;

auto load(const string& filename) -> std::tuple<unique_ptr<char*>, string_view>;
auto aggregate(string_view payload, unsigned min = 4) -> WordFreqs;

auto operator <<(std::ostream& os, WordFreqs const& wf) -> std::ostream& {
    for (auto [word, freq]: wf) os << std::setw(12) << word << ": " << freq << "\n";
//    for (auto pair: wf) os << std::setw(12) << pair.first << ": " << pair.second << "\n";
    return os;
}

int main() {
    auto startTime = chr::steady_clock::now();
    auto filename    = "../data/musketeers.txt"s;
    auto minWordSize = 4U;

    auto [res, payload] = load(filename);
    cout << "loaded " << payload.size() << " chars\n";

    auto result      = aggregate(payload, minWordSize);
    cout << result << "\n";

    auto endTime = chr::steady_clock::now();
    auto elapsedTime = chr::duration_cast<chr::milliseconds>(endTime - startTime).count();
    cout << "elapsed " << elapsedTime << " ms\n";
}

auto load(const string& filename) -> std::tuple<unique_ptr<char*>, string_view> {
    auto f = std::ifstream{filename};
    if (!f) throw std::invalid_argument{"cannot open "s + filename};

    size_t size = f.seekg(0, std::ios_base::end).tellg();
    f.seekg(0);

    auto buf = new char[size + 1];
    f.read(buf, size);
    buf[size] = '\0';
    return make_tuple(std::make_unique<char*>(buf), string_view{buf, size});
}

auto aggregate(string_view payload, unsigned int min) -> WordFreqs {
    auto       f       = WordFreqs{};
    auto const letters = "abcdefghijklmnopqrstuvwxyz"sv;

    auto start = 0UL;
    do {
        start     = payload.find_first_of(letters, start);
        if (start == string_view::npos) break;

        auto end  = payload.find_first_not_of(letters, start);
        if (end == string_view::npos) break;

        auto word = payload.substr(start, end - start);
        start = end + 1;

        if (word.size() >= min) ++f[word];
    } while (start < payload.size());

    return f;
}

#include <iostream>
#include <fstream>
#include <string>
#include <stdexcept>
#include <functional>
#include <set>
#include <unordered_set>
#include <algorithm>
#include <iterator>
#include <chrono>
#include <cctype>

using namespace std::chrono;
using namespace std::literals;
using std::cout;
using std::string;

auto strip(string word) -> string;
auto lc(string word) -> string;
auto load(std::istream& in) -> std::unordered_multiset<string>;
auto operator <<(std::ostream& os, std::multiset<string> const& freqs) -> std::ostream&;
void elapsed(std::function<void()> const& stmts);

int main(int argc, char** argv) {
    auto filename = (argc == 1) ? "../data/musketeers.txt"s : argv[1];
    elapsed([filename] {
        auto file = std::ifstream{filename};
        if (!file) throw std::invalid_argument{"cannot open "s + filename};

        auto words = load(file);
        auto freqs = std::multiset<string>{words.begin(), words.end()};
        cout << freqs << "\n";
    });
}

auto load(std::istream& in) -> std::unordered_multiset<string> {
    auto      words = std::unordered_multiset<string>{};
    for (auto word  = ""s; in >> word;) {
        word = strip(word);
        if (word.size() >= 4) words.insert(lc(word));
    }
    return words;
}

auto lc(string word) -> string {
    transform(word.begin(), word.end(), word.begin(), [](auto ch) {
        return ::tolower(ch);
    });
    return word;
}

auto strip(string word) -> string {
    string result{};
    std::copy_if(word.begin(), word.end(), back_inserter(result), [](auto ch) {
        return ::isalpha(ch);
    });
    return result;
}

auto operator<<(std::ostream& os, const std::multiset<string>& freqs) -> std::ostream& {
    for (auto it = freqs.begin(); it != freqs.end();) {
        auto word  = *it;
        auto count = freqs.count(word);
        os << word << ": " << count << "\n";
        advance(it, count);
    }
    return os;
}

void elapsed(const std::function<void()>& stmts) {
    auto startTime = high_resolution_clock::now();
    stmts();
    auto endTime = high_resolution_clock::now();
    auto elapsed = duration_cast<milliseconds>(endTime - startTime);
    cout << "Elapsed Time: " << elapsed.count() << " ms\n";
}

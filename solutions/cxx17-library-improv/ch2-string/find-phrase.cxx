#include <iostream>
#include <string>
#include <stdexcept>
#include <tuple>

using std::cout;
using std::cin;
using std::string;
using namespace std::literals::string_literals;

// Usage:
// ./search Aramis < musketeers.txt

int main(int numArgs, char* args[]) {
    if (numArgs <= 1) {
        throw std::invalid_argument("usage: "s + args[0] + " {phrase} < {input}"s);
    }
    string phrase = args[1];

    for (auto [line, lineno] = make_tuple(""s, 1U); getline(cin, line); ++lineno) {
        if (line.find(phrase) != string::npos) {
            cout << lineno << ": " << line << "\n";
        }
    }
    return 0;
}

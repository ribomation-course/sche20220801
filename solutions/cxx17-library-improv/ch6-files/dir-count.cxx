#include <iostream>
#include <filesystem>
#include <string>
#include <chrono>
#include <functional>
#include "count.hxx"

namespace fs = std::filesystem;
namespace rm = ribomation::io;
using namespace std::literals::string_literals;
using std::cout;
using std::setw;

void elapsed(const std::function<void()>& stmts);

int main(int argc, char** argv) {
    auto dir = fs::path{argc == 1 ? "../.." : argv[1]};
    if (!fs::is_directory(dir)) { dir = fs::current_path(); }
    cout << "Dir: " << fs::canonical(dir) << "\n";

    elapsed([dir] {
        auto total = rm::Count("TOTAL"s);
        auto filecnt = 1U;
        for (auto const& e: fs::recursive_directory_iterator{dir}) {
            auto const& p = e.path();
            if (fs::is_regular_file(p) && rm::isTextFile(p)) {
                auto cnt = rm::Count{p};
                cout << setw(3) << filecnt << ") " << cnt << "\n";
                total += cnt;
                ++filecnt;
            }
        }
        cout << setw(5) << "" << total << "\n";
    });
}

void elapsed(const std::function<void()>& stmts) {
    namespace chr = std::chrono;
    
    auto startTime = chr::high_resolution_clock::now();
    stmts();
    auto endTime = chr::high_resolution_clock::now();
    auto elapsed = chr::duration_cast<chr::milliseconds>(endTime - startTime);
    cout << "Elapsed Time: " << elapsed.count() << " ms\n";
}

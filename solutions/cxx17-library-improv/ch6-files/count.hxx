#pragma once

#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <filesystem>
#include <string>
#include <utility>
#include <unordered_set>

namespace ribomation::io {
    namespace fs = std::filesystem;
    using namespace std::literals::string_literals;
    using std::string;

    struct Count {
        string const name;
        unsigned     lines = 0;
        unsigned     words = 0;
        unsigned     chars = 0;

        explicit Count(string name) : name{std::move(name)} {}
        explicit Count(const fs::path& file) : name{file.string()} { update(file); }

        void update(fs::path const& file) {
            auto        f = std::ifstream{file};
            for (string line; getline(f, line);) {
                ++lines;
                auto buf = std::istringstream{line};
                for (string word; buf >> word;) ++words;
            }
            chars += fs::file_size(file);
        }

        void update(const Count& cnt) {
            lines += cnt.lines;
            words += cnt.words;
            chars += cnt.chars;
        }

        void operator+=(const Count& cnt) { update(cnt); }
    };

    inline auto operator<<(std::ostream& os, const Count& cnt) -> std::ostream& {
        using std::setw;
        return os << setw(6) << cnt.lines
                  << setw(8) << cnt.words
                  << setw(10) << cnt.chars
                  << "\t" << cnt.name;
    }

    inline bool isTextFile(const fs::path& p) {
        if (p.string().find("CMake"s) != string::npos) return false;
        static const auto TEXT_EXT = std::unordered_set<string>{".txt", ".cxx", ".hxx"};
        return TEXT_EXT.count(p.extension().string());
    }

}

#include <iostream>
#include <iomanip>
#include <string>
#include <utility>
#include <vector>
#include <random>
#include "account-generator.hxx"

using namespace std::literals;
namespace rm = ribomation::util;
using std::cout;
using std::string;

struct Account {
    string const accno;
    int          balance{};
    float        rate{};
    Account() = default;

    Account(string accno, int balance, float rate)
            : accno(std::move(accno)), balance(balance), rate(rate) {}
};

auto operator<<(std::ostream& os, Account const& a) -> std::ostream& {
    return os << "Account{"
              << a.accno << ", SEK "
              << a.balance << ", "
              << std::setprecision(1) << std::fixed << a.rate << "%}";
}


auto mkAccount() -> Account {
    return {
            rm::nextAccno(rm::nextBankPrefix() + "-###-####-##"s),
            rm::nextBalance(),
            rm::nextInterestRate(.5F, 3.F)
    };
}

int main(int argc, char** argv) {
    auto const N        = argc == 1 ? 10U : std::stoi(argv[1]);
    
    auto       accounts = std::vector<Account>{};
    accounts.reserve(N);
    
    for (auto k = 0U; k < N; ++k) accounts.push_back(mkAccount());
    for (auto const& a: accounts) cout << a << "\n";
    
    auto totalBalance = 0LL;
    for (auto const& a: accounts) totalBalance += a.balance;
    cout << "Total balance: " << totalBalance << " kr\n";
}

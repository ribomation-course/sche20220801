#include "account-generator.hxx"
#include <random>
#include <algorithm>
#include <iterator>

namespace {
    using namespace std::literals::string_literals;
    
    auto r = std::random_device{};

    auto banks = std::vector<std::string>{
            "SEB"s, "HB"s, "SWB"s, "NB"s, "HSBC"s
    };
}

namespace ribomation::util {

    float nextInterestRate(float lb, float ub) {
        auto rates = std::uniform_real_distribution<float>{lb, ub};
        return rates(r); //rates.operator()(r)
    }

    int nextBalance(float mean, float stddev) {
        auto amounts = std::normal_distribution<float>{mean, stddev};
        return static_cast<int>(amounts(r));
    }

    string nextBankPrefix() {
        auto idx = std::uniform_int_distribution<size_t>{0, banks.size() - 1};
        return banks[idx(r)];
    }

    string nextAccno(string pattern) {
        auto digits = std::uniform_int_distribution<char>{'0', '9'};

        string result;
        result.reserve(pattern.size());
        std::transform(pattern.begin(), pattern.end(), std::back_inserter(result), [&](auto ch) {
            return (ch == '#') ? digits(r) : ch;
        });

        return result;
    }

}

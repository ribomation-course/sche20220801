#include <iostream>
#include <string>
#include <regex>
using namespace std;

//usage: date | ./regex
int main() {
    auto re = regex{"\\d+"};
    for (string line; getline(cin, line);) {
       auto result = regex_replace(line, re, "#");
       cout << result << "\n";
    }
}

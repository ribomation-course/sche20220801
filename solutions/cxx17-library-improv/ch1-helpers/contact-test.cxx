#include "contact.hxx"

using namespace ribomation::domain;

int main() {
    Contact contacts[] = {
            Contact{"Anna Conda"s},

            Contact{"Justin Time"s}
                    .setEmail("justin@foobar.com"s),

            Contact{"Per Silja"s}
                    .setAddress("42 Reboot Lane", 12345, "PWA"),

            Contact{"Jens Riboe"}
                    .setEmail("jens.riboe@ribomation.se"s)
                    .setUrl("https://www.ribomation.se/"s)
                    .setAddress("Östermalmstorg 1"s, 11442, "Stockholm"s),
    };

    unsigned id = 1;
    for (auto& c: contacts)
        std::cout << "Contact " << id++ << ":\n" << c << "\n\n";
}

#pragma once

#include <iostream>
#include <string>
#include <optional>


namespace ribomation::domain {
    using namespace std::literals;
    using std::string;
    using std::optional;
    
    struct Address {
        optional<string>   street;
        optional<unsigned> postCode;
        optional<string>   city;
    };

    class Contact {
        string            name;
        optional<string>  email;
        optional<string>  url;
        optional<Address> address;
    public:
        Contact(string name) : name{name} {}

        ~Contact() = default;
        Contact(const Contact&) = default;
        Contact& operator=(const Contact&) = default;

        string getName() const { return name; }

        optional<string> getEmail() const { return email; }

        optional<string> getUrl() const { return url; }

        optional<Address> getAddress() const { return address; }

        Contact& setName(string name_) {
            Contact::name = name_;
            return *this;
        }

        Contact& setEmail(string email_) {
            Contact::email = email_;
            return *this;
        }

        Contact& setUrl(string url_) {
            Contact::url = url_;
            return *this;
        }

        Contact& setAddress(string street, unsigned postCode, string city) {
            Contact::address = {street, postCode, city};
            return *this;
        }

        friend std::ostream& operator<<(std::ostream& os, const Contact& c) {
            os << "name=" << c.name;
            if (c.email) //c.email.operator bool()
                os << "\nemail=" << c.email.value();
            if (c.url.has_value())
                os << "\nurl=" << *c.url;
            if (c.address)
                os << "\n"
                   << c.address.value().street.value()
                   << ", " << c.address.value().postCode.value()
                   << ", " << c.address.value().city.value();
            return os;
        }
    };

}
#include <iostream>
#include <type_traits>

using std::cout;

template<typename T, typename... Args>
T multiply(T arg, Args... args) {
    static_assert(std::is_arithmetic_v<T>, "Type must be integral or floating point");
    // if constexpr (sizeof...(args) == 0) return arg;
    return (arg * ... * args);
}

int main() {
    cout << "one = " << multiply(42) << "\n";
    cout << "10! = " << multiply(1,2,3,4,5,6,7,8,9,10) << "\n";
    cout << "dbl = " << multiply(1.5, 2.5, 3.5) << "\n";
//    cout << "char* = " << multiply("hepp") << "\n"; //error: static assertion failed: Type must be integral or floating point
}

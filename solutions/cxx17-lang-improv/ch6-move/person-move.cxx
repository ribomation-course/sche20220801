#include <iostream>
#include <string>
#include <cstring>

using std::cout;
using std::string;
using namespace std::literals::string_literals;

class Person {
    char* name;
public:
    Person() : name{::strdup("*")} {}

    Person(char const* name_) : name{::strdup(name_)} {}

    ~Person() { ::free(name); }

    Person(Person const&) = delete;
    auto operator=(Person const&) -> Person& = delete;

    Person(Person&& rhs)  noexcept : name{rhs.name} {
        rhs.name =nullptr; //::strdup("*");
    }

    auto operator=(Person&& rhs)  noexcept -> Person& {
        if (this != &rhs) {
            ::free(name);
            name = rhs.name;
            rhs.name = nullptr; //::strdup("*");
        }
        return *this;
    }
    
   // friend auto operator <<(std::ostream& os, Person const& p) -> std::ostream&;
   
   friend auto operator <<(std::ostream& os, Person const& p) -> std::ostream& {
       return os << "Person{" << p.name << "}";
   }
};

void byRef(Person const& p) {
    cout << "[byRef] " << p << "\n";
}
void byCopy(Person p) {
    cout << "[byRef] " << p << "\n";
}

int main() {
    auto pelle = Person{"Pelle"};
    cout << "person: " << pelle << "\n";
    byRef(pelle);
    byCopy(std::move(pelle));
    cout << "person: " << pelle << "\n";
}

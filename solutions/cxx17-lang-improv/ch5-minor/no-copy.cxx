#include <iostream>
#include <string>

using std::cout;
using std::string;
using namespace std::literals::string_literals;

struct Account {
    string accno{"undef"s};
    int    balance{};
    
    Account() = default;
    Account(string accno_, int balance_)
            : accno{std::move(accno_)}, balance{balance_}
            {}
    Account(Account const&) = delete;   
    auto operator =(Account const&) -> Account& = delete;   
};

void byValue(Account acc) {}
void byRef(Account const& acc) {}

int main() {
    auto a = Account{"ABC-123-456789"s, 120};
   // byValue(a);  //error: use of deleted function ‘Account::Account(const Account&)’
    byRef(a);
    auto b = Account{};
   // b = a; //error: use of deleted function ‘Account& Account::operator=(const Account&)’
}

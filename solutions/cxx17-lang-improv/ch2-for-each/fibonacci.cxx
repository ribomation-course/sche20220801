#include <iostream>

using std::cout;

class Fibonacci {
    const unsigned max_count;
public:
    Fibonacci(unsigned max_count_) : max_count{max_count_} {}

    struct iterator {
        unsigned long f0 = 0, f1 = 1;
        unsigned n = 0;

        iterator(unsigned max_count_) : n{max_count_} {}
        iterator() = default;

        bool operator!=(iterator const&) const { return n > 0; }
        unsigned long operator*() const { return f1; }
        void operator++() {
            unsigned long f = f0 + f1;
            f0 = f1;
            f1 = f;
            --n;
        }

    };

    iterator begin() const { return {max_count}; }
    iterator end() const { return {}; }
};

int main() {
    for (auto f: Fibonacci{91}) cout << f << " ";
}

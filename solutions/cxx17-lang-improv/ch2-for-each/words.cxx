#include <iostream>
#include <string>
#include <map>
#include <utility>

using std::cout;
using std::cin;
using std::string;
using std::map;

int main() {
    map<string, unsigned> freqs{};

    for (string word; cin >> word;) ++freqs[word];
    
    for (std::pair<string, unsigned> p: freqs)
        cout << p.first << ": " << p.second << "\n";
}

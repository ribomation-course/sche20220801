#include <iostream>
#include <string>
#include <set>

using std::cout;
using std::string;
using std::set;
using namespace std::literals::string_literals;

int main() {
    set<string> words = {"hello"s, "from"s, "C++"s};
    
    for (string const& w: words) cout << w << "\n";
}

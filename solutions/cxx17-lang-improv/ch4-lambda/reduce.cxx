#include <iostream>
#include <functional>

using namespace std;

auto reduce(int* arr, int N, function<int(int, int)> f) {
    int result = arr[0];

    for (auto k = 1; k < N; ++k) // result = result + arr[k]
        result = f(result, arr[k]);

    return result;
}

int main() {
    int       numbers[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    int const N         = sizeof(numbers) / sizeof(numbers[0]);
    for_each(&numbers[0], &numbers[N], [](auto n) { cout << n << " "; });
    cout << "\n";

    auto sum = reduce(numbers, N, [](int acc, int n) { return acc + n; });
    cout << "sum: " << sum << "\n";

    auto prod = reduce(numbers, N, [](int acc, int n) { return acc * n; });
    cout << "prod: " << prod << "\n";

    auto maxVal = reduce(numbers, N, [](int acc, int n) {
        return n > acc ? n : acc;
    });
    cout << "maxVal: " << maxVal << "\n";
}

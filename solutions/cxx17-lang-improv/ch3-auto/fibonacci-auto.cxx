#include <iostream>
#include <string>
#include <map>

using std::cout;

auto fib(unsigned n) {
    if (n == 0) return 0UL;
    if (n == 1) return 1UL;
    return fib(n - 2) + fib(n - 1);
}

struct Pair {
    unsigned      arg;
    unsigned long result;
};

auto compute(unsigned n) -> Pair;

auto fibonacci(unsigned n) -> std::map<unsigned, unsigned long> {
    auto result = std::map<unsigned, unsigned long>{};
    
    for (auto k = 1U; k <= n; ++k) {
        auto [a, f] = compute(k);
        result[a] = f;
    }
    return result;
}

int main(int argc, char** argv) {
    auto N = argc == 1 ? 42 : std::stoi(argv[1]);
    {
        auto f = fib(N);
        cout << "fib(" << N << ") = " << f << "\n";
    }
    {
        auto [n, f] = compute(N);
        cout << "fib(" << n << ") = " << f << "\n";
    }
    {
        cout << "---\n";
        for (auto [n, f]: fibonacci(N))
            cout << "fib(" << n << ") = " << f << "\n";
    }
}

auto compute(unsigned int n) -> Pair {
    return {n, fib(n)};
}
